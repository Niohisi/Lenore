#!/usr/bin/python
# coding=utf-8
import sys
import time
from datetime import datetime
import os
import re

import telebot
from telebot.types import InlineKeyboardMarkup, InlineKeyboardButton

import db_func

if len(sys.argv) < 2:
    print("Usage:")
    print("  lenore.py <bot_token>")
    exit(1)

###
### Переменные
###
bot_token = sys.argv[1]  # Lenore token
lenore = telebot.TeleBot(bot_token)


###
### Сервисные проверки
###
def check_user_is_admin(user_id, chat_id):
    """
    check_user_is_admin(message.from_user.id, message.chat.id)
    :rtype: Bool
    """
    foo = lenore.get_chat_administrators(chat_id)
    current_chat_administrators = []
    for user in foo:
        tmp = user.user.id
        current_chat_administrators.append(tmp)
    if user_id not in current_chat_administrators:
        return False
    else:
        return True


def check_chat_forwarding(cid):
    chat_pairs = {}
    # -1001032838103 furrygamers
    # 'chat_1001394778416] пм бот-чат
    chat_pairs['-1001450465443'] = '-1001450465443'  # lenoretest
    chat_pairs['-1001444879250'] = '-1001394778416'  # furmoscow
    chat_pairs['-1001457973105'] = '-1001394778416'  # afterdark
    # -1001394778416 - ПМ-бот-чат
    # -1001254199480 - оргчат ПМ
    # -1001328989582 - https://t.me/furrychatnsfw
    # -1001295499832 - https://t.me/furrydevnull

    if chat_pairs.get(str(cid)) is not None:
        foo = chat_pairs.get(str(cid))
        return foo
    else:
        return False


def check_bot_can_restrict(message):
    if lenore.get_chat_member(message.chat.id, lenore.get_me().id).can_restrict_members:
        return True
    else:
        return False


def check_bot_can_delete(message):
    if lenore.get_chat_member(message.chat.id, lenore.get_me().id).can_delete_messages:
        return True
    else:
        return False


def check_bot_can_pin(message):
    if lenore.get_chat_member(message.chat.id, lenore.get_me().id).can_pin_messages:
        return True
    else:
        return False


def check_bot_can_send_messages(message):
    if lenore.get_chat_member(message.chat.id, lenore.get_me().id).can_send_messages is None:
        return True
    else:
        return False


def check_bot_can_send_media(message):
    if lenore.get_chat_member(message.chat.id, lenore.get_me().id).can_send_media_messages is None:
        return True
    else:
        return False


def check_bot_can_change_info(message):
    if lenore.get_chat_member(message.chat.id, lenore.get_me().id).can_change_info is None:
        return True
    else:
        return False


def check_bot_can_promote_members(message):
    if lenore.get_chat_member(message.chat.id, lenore.get_me().id).can_promote_members is None:
        return True
    else:
        return False


def info_get_current_username(chat_id, user_id):
    foo = lenore.get_chat_member(chat_id, user_id).user
    if foo.username is not None:
        bar = '@' + foo.username
    else:
        try:
            bar = foo.first_name
        except:
            bar = foo.id
    return bar


###
### Обработка новых пользователей
###
@lenore.message_handler(content_types=["new_chat_members"])
def processing_anti_bot(message):
    try:
        incoming_user_name = info_get_current_username(message.chat.id, message.new_chat_member.id)
        protected_chats_list = [-1001444879250, -1001457973105, -1001032838103, -1001450465443, -1001085305161,
                                -1001080419111, -1001328989582]
        if db_func.db_check_chat_table_exists(message.chat.id):
            if db_func.db_check_user_exists(message.chat.id, message.new_chat_member.id):
                # -1001032838103 furrygamers
                # -1001450465443 lenoretest
                # -1001444879250 furmoscow
                # -1001457973105 afterdark
                # -1001085305161 vaporspace
                # -1001080419111 SG Club
                # -1001328989582 https://t.me/furrychatnsfw
                if message.chat.id in protected_chats_list:
                    if message.chat.id == -1001444879250:
                        welcome_message = 'С возвращением в чат Пушистой Москвы, {0}! \nМы скучали! Ну, большинство из нас. Наверно :D'.format(
                            incoming_user_name)
                    elif message.chat.id == -1001457973105:
                        welcome_message = 'С возвращением в уголок разврата - afterdark-чат Пушистой Москвы, {0}! \nТы очень многое пропустил ;)'.format(
                            incoming_user_name)
                    elif message.chat.id == -1001032838103:
                        welcome_message = 'С возвращением в Furry Gamers, {0}! \nМы скучали! Ну, большинство из нас. Наверно :D'.format(
                            incoming_user_name)
                    elif message.chat.id == -1001450465443:
                        welcome_message = 'С возвращением в чат для тестирования Ленор, {0}! \nМы скучали! Ну, большинство из нас. Наверно :D'.format(
                            incoming_user_name)
                    elif message.chat.id == -1001085305161:
                        welcome_message = 'С возвращением в #Vaporspace (SFW) (RU), {0}! \nМы скучали! Ну, большинство из нас. Наверно :D'.format(
                            incoming_user_name)
                    elif message.chat.id == -1001080419111:
                        welcome_message = "С возвращением в чат SG Club!"
                    elif message.chat.id == -1001328989582:
                        welcome_message = "С возвращением в уютный Фурри-чатик! :3"
                    lenore.send_message(message.chat.id, welcome_message)
            else:
                db_func.db_add_new_user(message.chat.id, message.new_chat_member.id, incoming_user_name)
                # -1001032838103 furrygamers
                # -1001450465443 lenoretest
                # -1001444879250 furmoscow
                # -1001457973105 afterdark
                # -1001085305161 vaporspace
                # -1001080419111 SG Club
                if message.chat.id in protected_chats_list:
                    lenore.restrict_chat_member(message.chat.id, message.new_chat_member.id, int(time.time()), False,
                                                False,
                                                False, False)
                    approve_data = str(message.new_chat_member.id)
                    markup = InlineKeyboardMarkup()
                    markup.add(InlineKeyboardButton("🦐", callback_data=approve_data))
                    welcome_message = ''
                    if message.chat.id == -1001444879250:
                        welcome_message = 'Добро пожаловать в чат Пушистой Москвы, {0}! \nПожалуйста, нажми 🦐, чтобы пройти верификацию.'.format(
                            incoming_user_name)
                    elif message.chat.id == -1001457973105:
                        welcome_message = 'Добро пожаловать в уголок разврата - afterdark-чат Пушистой Москвы, {0}! \nНажми на 🦐, чтобы подтвердить, что тебе есть 18 лет и что ты не бот.'.format(
                            incoming_user_name)
                    elif message.chat.id == -1001032838103:
                        welcome_message = 'Добро пожаловать в Furry Gamers, {0}! \nПожалуйста, нажми 🦐, чтобы пройти верификацию.'.format(
                            incoming_user_name)
                    elif message.chat.id == -1001450465443:
                        welcome_message = 'Добро пожаловать в чат для тестирования Ленор, {0}! \nПожалуйста, нажми 🦐, чтобы пройти верификацию.'.format(
                            incoming_user_name)
                    elif message.chat.id == -1001085305161:
                        welcome_message = 'Добро пожаловать в #Vaporspace (SFW) (RU), {0}! \nПожалуйста, нажми 🦐, чтобы пройти верификацию.'.format(
                            incoming_user_name)
                    elif message.chat.id == -1001080419111:
                        welcome_message = "Добро пожаловать в чат SG Club. Не забудь прочитать правила :) " \
                                          "\nhttps://telegra.ph/A-menya-ne-zabanyat-03-10 " \
                                          "\nЧтобы подтвердить, что ты не бот - нажми креветочку.".format(
                            incoming_user_name)
                    elif message.chat.id == -1001328989582:
                        welcome_message = 'Добро пожаловать в Фурри-чатик (NSFW) +18, {0}! \nПожалуйста, нажми на 🦐, чтобы подтвердить, что тебе есть 18 лет и что ты не бот.'.format(
                            incoming_user_name)
                    lenore.send_message(message.chat.id, welcome_message, reply_markup=markup)
        else:
            pass
    except Exception as e:
        lenore.send_message(message.chat.id, e)


###
### Обрабатываем /rate
###
@lenore.message_handler(content_types=['photo'])
def all_rate_photo(message):
    try:
        uid = message.from_user.id
        cid = message.chat.id
        username = info_get_current_username(cid, uid)
        if db_func.db_check_chat_table_exists(cid):
            if db_func.db_check_user_exists(cid, uid):
                db_func.db_update_user_message_count(cid, uid)
            else:
                db_func.db_add_new_user(cid, uid, username)
                db_func.db_update_user_message_count(cid, uid)
        if db_func.db_check_user_have_right(cid, uid, 'all_actions_allowed'):
            if message.caption == '/rate':
                lenore.delete_message(cid, message.message_id)
                file_info = lenore.get_file(message.photo[len(message.photo) - 1].file_id)
                rate_markup = InlineKeyboardMarkup()
                rate_markup.row_width == 1
                callback_upvote = 'upvote_photo_{0}_{1}'.format(0, 0)
                callback_downvote = 'downvote_photo_{0}_{1}'.format(0, 0)
                rate_markup.add(InlineKeyboardButton("0 👍", callback_data=callback_upvote),
                                InlineKeyboardButton("0 👎", callback_data=callback_downvote))
                photo_caption = '{0} запостил фото на оценку!✨'.format(username)
                lenore.send_photo(cid, file_info.file_id, caption=photo_caption, reply_markup=rate_markup)
        else:
            lenore.reply_to(message, "I'm sorry Dave, I'm afraid I can't do that.")
    except Exception as e:
        lenore.send_message(message.chat.id, e)


@lenore.callback_query_handler(func=lambda call: True)
def callback_inline(call):
    try:
        mid = call.message.message_id
        clicking_user = str(call.from_user.id)
        if call.data == clicking_user:
            welcome_message = ''
            if call.message.chat.id == -1001444879250:  # ПМ
                welcome_message = 'Привет, {0}!\n' \
                                  'Добро пожаловать в "Пушистую Москву"!\n' \
                                  'У нас можно найти: разные тусовки, клевое общение и ламповую обстановку.\n' \
                                  'Но у нас есть правила и их стоит соблюдать:\n ' \
                                  'https://telegra.ph/Pushistaya-Moskva-Pravila-03-11'.format(
                    call.from_user.first_name)
            elif call.message.chat.id == -1001457973105:  # ПМ афтердарк
                welcome_message = 'Что ж, нажатием ты подтвердил, что имеешь право тут находиться, {0}!\n' \
                                  'Правил как таковых тут нет, запрещены только extreme-фетиши (типа guro, scat, vore, и т.д.).\n' \
                                  'IRL-контент допустим и приветствуется. Enjoy.'.format(
                    call.from_user.first_name)
            elif call.message.chat.id == -1001032838103:  # ФГ
                welcome_message = "Добро пожаловать в Furry Gamers, {0}! " \
                                  "Чатик создан для обсуждения игр и всего, что с ними связано. " \
                                  "Умеренный флуд и оффтопик допустимы, но не скатывайся.\n" \
                                  "Уровень модерации и форма правления - просвещенная диктатура." \
                                  "\nВсе варны субъективны и выдаются по желанию левой пятки, " \
                                  "зависят от Луны в Водолее.\nВ целом правило одно — " \
                                  "каждый имеет право на свое личное мнение, но don't be a dick.\n" \
                                  "Добавлять в чат можно и нужно, но помните о том, что написано выше и ниже.\n" \
                                  "И one more thing — чат в основном фурревый и nsfw.".format(call.from_user.first_name)
            elif call.message.chat.id == -1001450465443:  # ленор тестчат
                welcome_message = 'Добро пожаловать в чат для тестирования Ленор, {0}!'.format(
                    call.from_user.first_name)
            elif call.message.chat.id == -1001085305161:  # vaporspace
                welcome_message = 'Что ж, ты не бот, {0}! \nПомним о тематике чата и ведем себя хорошо~'.format(
                    call.from_user.first_name)
            elif call.message.chat.id == -1001080419111:  # SG Club
                welcome_message = "Спасибо, верификация успешно пройдена! \nПравила: https://telegra.ph/A-menya-ne-zabanyat-03-10".format(
                    call.from_user.first_name)
            elif call.message.chat.id == -1001080419111:  # SG Club
                welcome_message = "Спасибо, верификация успешно пройдена! Веди себя хорошо и будь някой :3 \nПравила: https://telegra.ph/Furri-chatik-NSFW-pravila-06-22".format(
                    call.from_user.first_name)
            lenore.edit_message_text(welcome_message, call.message.chat.id, mid)
            lenore.answer_callback_query(callback_query_id=call.id, show_alert=True, text="Верификация пройдена")
            lenore.restrict_chat_member(call.message.chat.id, call.from_user.id, int(time.time()), True, True, True,
                                        True)
    except Exception as e:
        lenore.send_message(call.chat.id, e)

    try:
        splitted_call = call.data.split('_')
        cid = call.message.chat.id
        uid = call.from_user.id
        username = info_get_current_username(call.message.chat.id, call.from_user.id)
        if splitted_call[0] == 'upvote' or splitted_call[0] == 'downvote':
            if db_func.db_check_chat_table_exists(cid):
                if not db_func.db_check_user_exists(cid, uid):
                    db_func.db_add_new_user(cid, uid, username)
            upvote = int(splitted_call[2])
            downvote = int(splitted_call[3])
            photo_author = call.message.caption.split(' ')[0]
            list_of_voted_users = call.message.caption.split('✨')
            currently_voting_user = info_get_current_username(call.message.chat.id, call.from_user.id)
            voted_users = list_of_voted_users[1]
            if currently_voting_user not in voted_users:
                if splitted_call[0] == 'upvote':
                    upvoted_rate_markup = InlineKeyboardMarkup()
                    upvoted_rate_markup.row_width == 1
                    callback_upvote = 'upvote_photo_{0}_{1}'.format(str(upvote + 1), downvote)
                    upvote_caption = "{0} 👍".format(upvote + 1)

                    callback_downvote = 'downvote_photo_{0}_{1}'.format(str(upvote + 1), downvote)
                    downvote_caption = "{0} 👎".format(downvote)
                    if len(voted_users) > 0:
                        voted_users += ', ' + currently_voting_user
                    else:
                        voted_users += 'Проголосовали: ' + currently_voting_user

                    upvoted_rate_markup.add(
                        InlineKeyboardButton(upvote_caption, callback_data=callback_upvote),
                        InlineKeyboardButton(downvote_caption, callback_data=callback_downvote))
                    lenore.edit_message_caption(
                        '{0} запостил фото на оценку!✨ {1}'.format(photo_author, voted_users),
                        call.message.chat.id, mid)
                    lenore.edit_message_reply_markup(call.message.chat.id, mid, reply_markup=upvoted_rate_markup)
                    lenore.answer_callback_query(callback_query_id=call.id, show_alert=False, text="Upvoted")
                elif splitted_call[0] == 'downvote':
                    downvoted_rate_markup = InlineKeyboardMarkup()
                    downvoted_rate_markup.row_width == 1
                    callback_upvote = 'upvote_photo_{0}_{1}'.format(upvote, str(downvote + 1))
                    upvote_caption = "{0} 👍".format(upvote)

                    callback_downvote = 'downvote_photo_{0}_{1}'.format(upvote, str(downvote + 1))
                    downvote_caption = "{0} 👎".format(downvote + 1)

                    if len(voted_users) > 0:
                        voted_users += ', ' + currently_voting_user
                    else:
                        voted_users += 'Проголосовали: ' + currently_voting_user

                    downvoted_rate_markup.add(
                        InlineKeyboardButton(upvote_caption, callback_data=callback_upvote),
                        InlineKeyboardButton(downvote_caption, callback_data=callback_downvote))

                    lenore.edit_message_caption(
                        '{0} запостил фото на оценку!✨ {1}'.format(photo_author, voted_users),
                        call.message.chat.id, mid)
                    lenore.edit_message_reply_markup(call.message.chat.id, mid, reply_markup=downvoted_rate_markup)
                    lenore.answer_callback_query(callback_query_id=call.id, show_alert=False, text="Downvoted")
            else:
                lenore.answer_callback_query(callback_query_id=call.id, show_alert=False, text="Ты уже проголосовал!")
    except Exception as e:
        lenore.send_message(call.chat.id, e)


###
### Команды на действия, доступные всем
###

@lenore.message_handler(commands=['userinfo'])
def all_userinfo(message):
    try:
        cid = message.chat.id
        if message.reply_to_message is None:
            uid = message.from_user.id
        else:
            uid = message.reply_to_message.from_user.id

        if db_func.db_check_chat_table_exists(cid):
            if not db_func.db_check_user_exists(cid, uid):
                db_func.db_add_new_user(cid, uid, info_get_current_username(cid, uid))
            if db_func.db_check_user_have_right(cid, uid, 'all_actions_allowed'):
                user_rights = db_func.db_check_all_user_rights(cid, uid)
                infostring = "Пользователь `{0}`:\n" \
                             "Количество сообщений: `{1}`\n" \
                             "Количество предупреждений: `{2}`\n" \
                             "Последнее предупреждение: `{3}`\n" \
                             "Доступы: `{4}{5}{6}{7}{8}{9}{10}`".format(
                    info_get_current_username(cid, uid),
                    db_func.db_get_message_count(cid, uid)[0],
                    db_func.db_get_current_warn_info(cid, uid)[0],
                    db_func.db_get_current_warn_info(cid, uid)[1],
                    user_rights[0],
                    user_rights[1],
                    user_rights[2],
                    user_rights[3],
                    user_rights[4],
                    user_rights[5],
                    user_rights[6])
                lenore.reply_to(message, infostring, parse_mode='Markdown')
            else:
                lenore.reply_to(message, "I'm sorry Dave, I'm afraid I can't do that.")

        else:
            lenore.send_message(cid, 'Поскольку база для чата не создавалась, данных нет, увы ¯\_(ツ)_/¯')
    except Exception as e:
        lenore.send_message(message.chat.id, e)


@lenore.message_handler(commands=['slap'])
def all_slap(message):
    try:
        cid = message.chat.id
        uid = message.from_user.id
        if db_func.db_check_chat_table_exists(cid):
            if not db_func.db_check_user_exists(cid, uid):
                db_func.db_add_new_user(cid, uid, info_get_current_username(cid, uid))
            if db_func.db_check_user_have_right(cid, uid, 'all_actions_allowed'):
                text = message.text
                spl = text.split(' ')
                user_from = info_get_current_username(message.chat.id, message.from_user.id)
                msg_text = ''
                lenore.delete_message(message.chat.id, message.message_id)
                if len(spl) == 1:
                    msg_text += user_from + ' slaps himself around a bit with a large trout'
                    lenore.send_message(message.chat.id, msg_text)
                else:
                    user_slapped = spl[1]
                    msg_text += user_from + ' slaps ' + user_slapped + ' around a bit with a large trout'
                    lenore.send_message(message.chat.id, msg_text)
            else:
                lenore.reply_to(message, "I'm sorry Dave, I'm afraid I can't do that.")
    except Exception as e:
        lenore.send_message(message.chat.id, e)


@lenore.message_handler(commands=['me'])
def all_me_action(message):
    try:
        cid = message.chat.id
        uid = message.from_user.id
        if db_func.db_check_chat_table_exists(cid):
            if not db_func.db_check_user_exists(cid, uid):
                db_func.db_add_new_user(cid, uid, info_get_current_username(cid, uid))
            if db_func.db_check_user_have_right(cid, uid, 'all_actions_allowed'):
                text = message.text
                spl = text.split(' ')
                user_from = info_get_current_username(message.chat.id, message.from_user.id)
                msg_text = ''
                lenore.delete_message(message.chat.id, message.message_id)
                if len(spl) == 1:
                    msg_text += user_from + ' делает что-то подозрительное...'
                    lenore.send_message(message.chat.id, msg_text)
                else:
                    user_action = ' '.join(spl[1:])
                    msg_text += user_from + ' ' + user_action
                    lenore.send_message(message.chat.id, msg_text)
            else:
                lenore.reply_to(message, "I'm sorry Dave, I'm afraid I can't do that.")
    except Exception as e:
        lenore.send_message(message.chat.id, e)


@lenore.message_handler(commands=['topmsg'])
def all_topmsg(message):
    try:
        cid = message.chat.id
        uid = message.from_user.id
        if db_func.db_check_chat_table_exists(cid):
            if not db_func.db_check_user_exists(cid, uid):
                db_func.db_add_new_user(cid, uid, info_get_current_username(cid, uid))
            if db_func.db_check_user_have_right(cid, uid, 'all_actions_allowed'):
                result_query = db_func.db_get_top_flooders(cid)
                output = 'Итак, топ-5 флудеров группы:\n'
                for idx, data in enumerate(result_query, start=0):
                    foo = "`{0}` - `{1}`\n".format(data[0], data[1])
                    output += foo
                    if idx == 4:
                        break
                lenore.reply_to(message, output, parse_mode='Markdown')
            else:
                lenore.reply_to(message, "I'm sorry Dave, I'm afraid I can't do that.")
    except Exception as e:
        lenore.send_message(message.chat.id, e)


@lenore.message_handler(commands=['report'])
def all_report(message):
    try:
        cid = message.chat.id  # ид чата
        uid = message.from_user.id  # ид отдающего команду
        if db_func.db_check_chat_table_exists(cid):
            if not db_func.db_check_user_exists(cid, uid):
                db_func.db_add_new_user(cid, uid, info_get_current_username(cid, uid))
            if db_func.db_check_user_have_right(cid, uid, 'all_actions_allowed'):
                if message.reply_to_message is None:
                    lenore.reply_to(message, 'Команду возможно использовать только ответом на сообщение!')
                else:
                    ruid = message.reply_to_message.from_user.id  # id юзера, на сообщение которого реплаят
                    rmid = message.reply_to_message.message_id  # id сообщения, на которое реплаят
                    if message.chat.username is None:
                        chat_link = '(приватный чат, ссылка недоступна)'
                    else:
                        chat_link = 't.me/' + message.chat.username

                    if not check_chat_forwarding(cid):
                        lenore.reply_to(message.reply_to_message, '@niohisi, тут в чатике что-то не так!')

                    else:
                        lenore.reply_to(message.reply_to_message, 'Сообщение передано модераторам.')
                        lenore.forward_message(check_chat_forwarding(cid), cid,
                                               rmid)
                        lenore.send_message(check_chat_forwarding(cid),
                                            '{0} жалуется на сообщение {1} в чате {2} ({3})'.format(
                                                info_get_current_username(cid, uid),
                                                info_get_current_username(cid, ruid),
                                                message.chat.title,
                                                chat_link), disable_web_page_preview=True)
            else:
                lenore.reply_to(message, "I'm sorry Dave, I'm afraid I can't do that.")
    except Exception as e:
        lenore.reply_to(message, e)


@lenore.message_handler(commands=['lenorehelp'])
def all_lenorehelp(message):
    welcome_text = """Команды бота:
        /report - жалоба на сообщение;
        /userinfo - ответом на сообщение сообщает количество сообщений и варном для автора сообщение. При использовании без реплая - статистику использовавшего;
        /me something - бот выводит сообщение вида @твой юзернейм something;
        /slap кто-то - бот выводит сообщение "<твой ник> slaps <кто-то> around a bit with a large trout";
        /topmsg - топ-5 юзеров по сообщениям в текущем чате
        /rate - оценка фото, если написать это в комментарии к отправленному фото. Одно за раз.
        /msk_fur - ссылка на чат "Пушистая Москва";
        /afterdark - ссылка на afterdark-чат "Пушистой Москвы" (18+) (работает только из основного чата);
        /furrygamers - cсылка на Furry gamers [RU] [18+];
        /vapefur - ссылка на #Vaporspace (SFW) (RU)"""

    try:
        cid = message.chat.id  # ид чата
        uid = message.from_user.id  # ид отдающего команду
        if db_func.db_check_chat_table_exists(cid):
            if not db_func.db_check_user_exists(cid, uid):
                db_func.db_add_new_user(cid, uid, info_get_current_username(cid, uid))
            if db_func.db_check_user_have_right(cid, uid, 'all_actions_allowed'):
                lenore.reply_to(message, welcome_text)
            else:
                lenore.reply_to(message, "I'm sorry Dave, I'm afraid I can't do that.")
    except Exception as e:
        lenore.send_message(message.chat.id, e)


###
### Линки на чатики
###
@lenore.message_handler(commands=['afterdark'])
def link_afterdark(message):
    try:
        if message.chat.id == -1001444879250:
            lenore.reply_to(message, 'Ссылка на afterdark-чат Пушистой Москвы. Внимание, чат 18+!: \n'
                                     'https://t.me/joinchat/AX0jxAwS6vipAuCUL0ickw')
        else:
            lenore.reply_to(message, 'Прошу прощения, запрос этой ссылки работает только из основного чата ПМ')
    except Exception as e:
        lenore.send_message(message.chat.id, e)


@lenore.message_handler(commands=['furrygamers'])
def link_furrygamers(message):
    try:
        lenore.reply_to(message, 'Ссылка на Furry gamers [RU] [18+]: \n'
                                 'https://t.me/FurryGS')
    except Exception as e:
        lenore.send_message(message.chat.id, e)


@lenore.message_handler(commands=['msk_fur'])
def link_msk_fur(message):
    try:
        lenore.reply_to(message, 'Ссылка на чат "Пушистая Москва": \n'
                                 'https://t.me/msk_fur')
    except Exception as e:
        lenore.send_message(message.chat.id, e)


@lenore.message_handler(commands=['vapefur'])
def link_vapefur(message):
    try:
        lenore.reply_to(message, 'Ссылка на #Vaporspace (SFW) (RU): \n'
                                 'https://t.me/vapefur')
    except Exception as e:
        lenore.send_message(message.chat.id, e)


# furry > /dev/null
@lenore.message_handler(commands=['furcoding'])
def link_furrydevnull(message):
    try:
        lenore.reply_to(message, 'Ссылка на чат русскоязычных фуррей-программистов "furry > /dev/null": \n'
                                 'https://t.me/furrydevnull')
    except Exception as e:
        lenore.send_message(message.chat.id, e)


###
### Команды на действия, доступные только модерам
###
@lenore.message_handler(commands=['eww'])
def mod_eww(message):
    try:
        cid = message.chat.id  # ид чата
        uid = message.from_user.id  # ид отдающего команду
        if db_func.db_check_chat_table_exists(cid):
            if not db_func.db_check_user_exists(cid, uid):
                db_func.db_add_new_user(cid, uid, info_get_current_username(cid, uid))
            if db_func.db_check_user_have_right(cid, uid, 'limited_actions_allowed'):
                dn = os.path.dirname(os.path.realpath(__file__))
                fn = os.path.join(dn, "eww.mp4")
                f = open(fn, 'rb')
                if message.reply_to_message is None:
                    lenore.delete_message(cid, message.message_id)
                    lenore.send_document(cid, f)
                else:
                    lenore.delete_message(cid, message.message_id)
                    lenore.send_document(cid, f, message.reply_to_message.message_id)
            else:
                lenore.reply_to(message, "I'm sorry Dave, I'm afraid I can't do that.")
    except Exception as e:
        lenore.send_message(message.chat.id, e)


@lenore.message_handler(commands=['usuka'])
def mod_usuka(message):
    try:
        cid = message.chat.id  # ид чата
        uid = message.from_user.id  # ид отдающего команду
        if db_func.db_check_chat_table_exists(cid):
            if not db_func.db_check_user_exists(cid, uid):
                db_func.db_add_new_user(cid, uid, info_get_current_username(cid, uid))
            if db_func.db_check_user_have_right(cid, uid, 'limited_actions_allowed'):
                dn = os.path.dirname(os.path.realpath(__file__))
                fn = os.path.join(dn, "usuka.webp")
                f = open(fn, 'rb')
                if message.reply_to_message is None:
                    lenore.delete_message(cid, message.message_id)
                    lenore.send_sticker(cid, f)
                else:
                    lenore.delete_message(cid, message.message_id)
                    lenore.send_sticker(cid, f, message.reply_to_message.message_id)
            else:
                lenore.reply_to(message, "I'm sorry Dave, I'm afraid I can't do that.")
    except Exception as e:
        lenore.send_message(message.chat.id, e)


@lenore.message_handler(commands=['wtfisgoingon'])
def mod_wtfisgoingon(message):
    try:
        cid = message.chat.id  # ид чата
        uid = message.from_user.id  # ид отдающего команду
        if db_func.db_check_chat_table_exists(cid):
            if not db_func.db_check_user_exists(cid, uid):
                db_func.db_add_new_user(cid, uid, info_get_current_username(cid, uid))
            if db_func.db_check_user_have_right(cid, uid, 'limited_actions_allowed'):
                dn = os.path.dirname(os.path.realpath(__file__))
                fn = os.path.join(dn, "wtfisgoingon.jpg")
                f = open(fn, 'rb')
                if message.reply_to_message is None:
                    lenore.delete_message(cid, message.message_id)
                    lenore.send_photo(cid, f, caption='')
                else:
                    lenore.delete_message(cid, message.message_id)
                    lenore.send_photo(cid, f, caption='', reply_to_message_id=message.reply_to_message.message_id)
            else:
                lenore.reply_to(message, "I'm sorry Dave, I'm afraid I can't do that.")
    except Exception as e:
        lenore.send_message(message.chat.id, e)


@lenore.message_handler(commands=['badumtss'])
def mod_badumtss(message):
    try:
        cid = message.chat.id  # ид чата
        uid = message.from_user.id  # ид отдающего команду
        if db_func.db_check_chat_table_exists(cid):
            if not db_func.db_check_user_exists(cid, uid):
                db_func.db_add_new_user(cid, uid, info_get_current_username(cid, uid))
            if db_func.db_check_user_have_right(cid, uid, 'limited_actions_allowed'):
                dn = os.path.dirname(os.path.realpath(__file__))
                fn = os.path.join(dn, "badumtss.png")
                f = open(fn, 'rb')
                if message.reply_to_message is None:
                    lenore.delete_message(cid, message.message_id)
                    lenore.send_sticker(cid, f)
                else:
                    lenore.delete_message(cid, message.message_id)
                    lenore.send_sticker(cid, f, message.reply_to_message.message_id)
            else:
                lenore.reply_to(message, "I'm sorry Dave, I'm afraid I can't do that.")
    except Exception as e:
        lenore.send_message(message.chat.id, e)


###
### Модераторские команды
###


@lenore.message_handler(commands=['warn'])
def mod_warn(message):
    try:
        cid = message.chat.id  # ид чата
        uid = message.from_user.id  # ид отдающего команду
        if db_func.db_check_chat_table_exists(cid):
            if not db_func.db_check_user_exists(cid, uid):
                db_func.db_add_new_user(cid, uid, info_get_current_username(cid, uid))
            if db_func.db_check_user_have_right(cid, uid, 'warn_func'):
                if message.reply_to_message is None:
                    lenore.reply_to(message, 'Команду возможно использовать только ответом на сообщение!')
                else:
                    ruid = message.reply_to_message.from_user.id  # id юзера, на сообщение которого реплаят
                    rmid = message.reply_to_message.message_id  # id сообщения, на которое реплаят
                    if check_user_is_admin(ruid, cid):
                        lenore.reply_to(message, 'Невозможно кинуть варну тому, кто сильнее меня, я простой бот. :(')
                    else:  # если не админ
                        spl = str(message.text).split(' ')  # получаем причину варна, если есть
                        if len(spl) == 1:
                            lenore.reply_to(message, 'Неверный синтаксис команды, бака!\n'
                                                     'Правильно: /warn [причина]')
                        else:
                            warn_reason = ' '.join(spl[1:])
                            warned_user_naming = info_get_current_username(cid,
                                                                           ruid)  # получаем видимое имя пользователя
                            if message.chat.username is None:  # получаем ссылку на чат, чтобы использовать дальше
                                chat_link = '(приватный чат, ссылка недоступна)'
                            else:
                                chat_link = 't.me/' + message.chat.username

                            if db_func.db_check_chat_table_exists(cid):  # проверяем, есть ли чат в базе данных
                                if db_func.db_check_user_exists(cid, ruid):  # если есть - проверяем наличие юзера
                                    current_warn_count = db_func.db_get_current_warn_info(cid, ruid)[
                                                             0] + 1  # получаем текущие варны с новым для отображения
                                    db_func.db_increase_warn_count(cid, ruid,
                                                                   warn_reason)  # увеличиваем количество варнов
                                else:  # пользователя нет, но чат в базе
                                    db_func.db_add_new_user(cid, ruid, warned_user_naming)  # заводим пользователя
                                    current_warn_count = db_func.db_get_current_warn_info(cid, ruid)[
                                                             0] + 1  # получаем текущие варны с новым для отображения
                                    db_func.db_increase_warn_count(cid, ruid,
                                                                   warn_reason)  # увеличиваем количество варнов

                                # сообщение для случая, если чат в базе
                                warn_message = """{0}, предупреждение!\nПричина: {1}\nТекущее количество предупреждений: {2}""".format(
                                    warned_user_naming,
                                    warn_reason,
                                    current_warn_count)
                                # сообщение для пересылки, если чат в базе
                                info_message_text = """{0} выдал варн пользователю {1} в чате {2} ({3})\nПричина: {4}\nТекущее количество предупреждений: {5}""".format(
                                    info_get_current_username(cid, uid),
                                    warned_user_naming,
                                    message.chat.title,
                                    chat_link, warn_reason, current_warn_count)

                            else:
                                # сообщение для случая, если чата нет в базе
                                warn_message = '{0}, предупреждение!\n' \
                                               'Причина: {1}'.format(warned_user_naming, warn_reason)
                                # сообщение для пересылки, если чата в базе нет
                                info_message_text = """{0} выдал варн пользователю {1} в чате {2} ({3})\nПричина: {4}""".format(
                                    info_get_current_username(cid, uid),
                                    warned_user_naming,
                                    message.chat.title,
                                    chat_link, warn_reason)

                            # если чата нет в списке на форвард, просто отправляем сообщение
                            if not check_chat_forwarding(cid):
                                lenore.reply_to(message.reply_to_message, warn_message)
                            # если чат есть в списке на форвард - отправляем сообщение с варном в чат и пересылаем куда надо
                            else:
                                lenore.reply_to(message.reply_to_message, warn_message)
                                lenore.forward_message(check_chat_forwarding(cid), cid,
                                                       rmid)
                                lenore.send_message(check_chat_forwarding(cid), info_message_text,
                                                    disable_web_page_preview=True)
            else:
                lenore.reply_to(message, "I'm sorry Dave, I'm afraid I can't do that.")
    except Exception as e:
        lenore.reply_to(message, e)


@lenore.message_handler(commands=['set_rights'])
def mod_set_rights(message):
    # all_actions_allowed, limited_actions_allowed, warn_func, mute_func, ban_func, pin_func, can_change_rights
    try:
        cid = message.chat.id  # ид чата
        uid = message.from_user.id  # ид отдающего команду
        mid = message.message_id  # id сообщения с командой
        if not check_user_is_admin(uid, cid):
            lenore.delete_message(cid, mid)
        else:
            if not db_func.db_check_chat_table_exists(cid):
                lenore.reply_to(message, "I'm sorry Dave, I'm afraid I can't do that.\nТаблицы для чата не существует.")
            else:
                if message.reply_to_message is None:
                    lenore.reply_to(message,
                                    "I'm sorry Dave, I'm afraid I can't do that.\nКоманда должна быть дана реплаем")
                else:
                    if not db_func.db_check_user_have_right(cid, uid, 'can_change_rights'):
                        lenore.reply_to(message,
                                        "I'm sorry Dave, I'm afraid I can't do that.\nУ тебя нет доступа на изменение прав.")
                    else:
                        command = str(message.text).split(' ')
                        if not re.match(r'[01]{7}\Z', command[1]):
                            lenore.reply_to(message,
                                            "I'm sorry Dave, I'm afraid I can't do that.\nНекорректный синтаксис. /set_rights [nnnnnnn], где n=0 или 1")
                        else:
                            ruid = message.reply_to_message.from_user.id  # id юзера, на сообщение которого реплаят
                            if not db_func.db_check_user_exists(cid, ruid):
                                db_func.db_add_new_user(cid, ruid, info_get_current_username(cid, ruid))
                            current_user_rights = db_func.db_check_all_user_rights(cid, ruid)
                            db_func.db_set_user_rights(cid, ruid, command[1][0], command[1][1], command[1][2],
                                                       command[1][3], command[1][4], command[1][5], command[1][6])
                            new_user_rights = db_func.db_check_all_user_rights(cid, ruid)
                            lenore.reply_to(message, 'Права успешно изменены для {0}:\n'
                                                     'Было:  {1}\n'
                                                     'Стало: {2}'.format(info_get_current_username(cid, ruid),
                                                                         current_user_rights,
                                                                         new_user_rights))
    except Exception as e:
        lenore.reply_to(message, e)


@lenore.message_handler(commands=['mute'])
def mod_mute(message):
    try:
        cid = message.chat.id  # ид чата
        uid = message.from_user.id  # ид отдающего команду
        if db_func.db_check_chat_table_exists(cid):
            if not db_func.db_check_user_exists(cid, uid):
                db_func.db_add_new_user(cid, uid, info_get_current_username(cid, uid))
            if db_func.db_check_user_have_right(cid, uid, 'mute_func'):
                if message.reply_to_message is None:
                    lenore.reply_to(message, 'Команду возможно использовать только ответом на сообщение!')
                else:
                    ruid = message.reply_to_message.from_user.id  # id юзера, на сообщение которого реплаят
                    rmid = message.reply_to_message.message_id  # id сообщения, на которое реплаят
                    if check_user_is_admin(ruid, cid):
                        lenore.reply_to(message, 'Невозможно наложить мут на того, кто сильнее меня, я простой бот. :(')
                    else:
                        command = str(message.text).split(' ')
                        if not re.match(r'((\d*\s)([dmh])(\s)(.*))', ' '.join(command[1:])):
                            lenore.reply_to(message, 'Неверный синтаксис команды, бака!\n'
                                                     'Правильно: /mute [time] [m/d/h] [причина]')
                        else:
                            mute_time = 60
                            if command[2] == 'd':
                                mute_time = int(command[1]) * 86400
                            elif command[2] == 'h':
                                mute_time = int(command[1]) * 3600
                            elif command[2] == 'm':
                                mute_time = int(command[1]) * 60
                            mute_reason = ' '.join(command[3:])
                            mute_until = int(time.time()) + mute_time
                            lenore.restrict_chat_member(cid, ruid,
                                                        mute_until, False, False,
                                                        False, False)
                            muted_user_naming = info_get_current_username(cid, ruid)
                            mute_ending_date = str(datetime.utcfromtimestamp(int(mute_until + 10800)).strftime(
                                '%Y-%m-%d %H:%M:%S'))
                            mute_text = """Поздравляю, {0}! На тебя наложена молчанка до {1}\nПричина (если еще не понятно): {2}""".format(
                                muted_user_naming, mute_ending_date,
                                mute_reason)
                            lenore.reply_to(message.reply_to_message, mute_text)

                            if check_chat_forwarding(cid):
                                if message.chat.username is None:
                                    chat_link = '(приватный чат, ссылка недоступна)'
                                else:
                                    chat_link = 't.me/' + message.chat.username

                                forward_message_text = """{0} наложил молчанку на {1} до {2} в чате {3} ({4})\nПричина: {5}""".format(
                                    info_get_current_username(cid, uid),
                                    muted_user_naming,
                                    mute_ending_date,
                                    message.chat.title,
                                    chat_link,
                                    mute_reason)

                                lenore.forward_message(check_chat_forwarding(cid), cid, rmid)
                                lenore.send_message(check_chat_forwarding(cid), forward_message_text,
                                                    disable_web_page_preview=True)
            else:
                lenore.reply_to(message, "I'm sorry Dave, I'm afraid I can't do that.")
    except Exception as e:
        lenore.reply_to(message, e)


@lenore.message_handler(commands=['ban'])
def mod_ban(message):
    try:
        cid = message.chat.id  # ид чата
        uid = message.from_user.id  # ид отдающего команду
        mid = message.message_id  # id сообщения с командой
        rmid = message.reply_to_message.message_id  # id
        if db_func.db_check_chat_table_exists(cid):
            if not db_func.db_check_user_exists(cid, uid):
                db_func.db_add_new_user(cid, uid, info_get_current_username(cid, uid))
            if db_func.db_check_user_have_right(cid, uid, 'ban_func'):
                if message.reply_to_message is None:
                    lenore.delete_message(cid, mid)
                else:
                    ruid = message.reply_to_message.from_user.id  # id юзера, на сообщение которого реплаят
                    if check_user_is_admin(ruid, cid):
                        lenore.reply_to(message, 'Админа нельзя забанить.')
                    else:
                        command = str(message.text).split(' ')
                        kicked_user_naming = info_get_current_username(cid, ruid)
                        if len(command) > 1:
                            kick_reason = ' '.join(command[1:])
                            lenore.kick_chat_member(cid, ruid)

                            if db_func.db_check_chat_table_exists(cid):  # проверяем, есть ли чат в базе данных
                                if db_func.db_check_user_exists(cid, ruid):  # если есть - проверяем наличие юзера
                                    db_func.db_add_ban_info(cid, ruid, kick_reason)
                                else:  # пользователя нет, но чат в базе
                                    db_func.db_add_new_user(cid, ruid, kicked_user_naming)  # заводим пользователя
                                    db_func.db_add_ban_info(cid, ruid, kick_reason)

                            kick_text = """{0} был забанен. \nПричина бана: {1}""".format(kicked_user_naming,
                                                                                          kick_reason)
                            lenore.reply_to(message, kick_text)
                            if check_chat_forwarding(cid):
                                if message.chat.username is None:
                                    chat_link = '(приватный чат, ссылка недоступна)'
                                else:
                                    chat_link = 't.me/' + message.chat.username
                                forward_message_text = """{0} забанил {1} в чате {2} ({3})\nПричина: {4}""".format(
                                    info_get_current_username(cid, uid),
                                    kicked_user_naming,
                                    message.chat.title,
                                    chat_link, kick_reason)
                                lenore.forward_message(check_chat_forwarding(cid), cid, rmid)
                                lenore.send_message(check_chat_forwarding(cid), forward_message_text,
                                                    disable_web_page_preview=True)

                        else:
                            lenore.reply_to(message, 'Необходимо указать причину бана!')
            else:
                lenore.reply_to(message, "I'm sorry Dave, I'm afraid I can't do that.")
    except Exception as e:
        lenore.reply_to(message, e)


@lenore.message_handler(commands=['nullifywarn'])
def mod_nullify_warn(message):
    try:
        cid = message.chat.id
        uid = message.from_user.id  # ид отдающего команду
        if db_func.db_check_chat_table_exists(cid):
            if not db_func.db_check_user_exists(cid, uid):
                db_func.db_add_new_user(cid, uid, info_get_current_username(cid, uid))
            if db_func.db_check_user_have_right(cid, uid, 'warn_func'):
                if message.reply_to_message is None:
                    lenore.delete_message(cid, message.message_id)
                else:
                    ruid = message.reply_to_message.from_user.id  # получаем цель, которую варним

                    if check_user_is_admin(ruid, cid):  # проверяем цель на админа
                        lenore.delete_message(cid, message.message_id)
                    else:  # если не админ  # удаляем исходное сообщение
                        unwarned_user_naming = info_get_current_username(cid, ruid)  # получаем видимое имя пользователя

                        if message.chat.username is None:  # получаем ссылку на чат, чтобы использовать дальше
                            chat_link = '(приватный чат, ссылка недоступна)'
                        else:
                            chat_link = 't.me/' + message.chat.username

                        if db_func.db_check_chat_table_exists(cid):  # проверяем, есть ли чат в базе данных
                            if db_func.db_get_current_warn_info(cid, ruid)[0] > 0:
                                if db_func.db_check_user_exists(cid, ruid):  # если есть - проверяем наличие юзера
                                    db_func.db_nullify_warn_count(cid, ruid)  # стираем варны

                                    # сообщение для случая, если чат в базе
                                    nullify_message = 'Предупреждения сняты для {0}\n' \
                                                      'Текущее количество предупреждений: 0'.format(
                                        unwarned_user_naming)
                                    # сообщение для пересылки, если чат в базе
                                    info_message_text = '{0} снял все предупреждения для {1} в чате {2} ({3})'.format(
                                        info_get_current_username(cid, message.from_user.id),
                                        unwarned_user_naming,
                                        message.chat.title,
                                        chat_link)
                                    # если чата нет в списке на форвард, просто отправляем сообщение
                                    if not check_chat_forwarding(message.chat.id):
                                        lenore.reply_to(message, nullify_message)
                                    # если чат есть в списке на форвард - отправляем сообщение с анварном в чат и пересылаем куда надо
                                    else:
                                        lenore.reply_to(message, nullify_message)
                                        lenore.forward_message(check_chat_forwarding(message.chat.id), message.chat.id,
                                                               message.reply_to_message.message_id)
                                        lenore.send_message(check_chat_forwarding(message.chat.id), info_message_text)
                                else:
                                    lenore.reply_to(message,
                                                    'Юзер {0} в базе не зарегистрирован!'.format(
                                                        unwarned_user_naming))
                            else:
                                lenore.reply_to(message,
                                                'Мне интересно, как ты собираешься снимать варны у {0}, если их вообще-то нет?'.format(
                                                    unwarned_user_naming))

                        else:
                            lenore.send_message(cid,
                                                'Поскольку чат не в базе, варны не считаются и снять их невозможно.')
            else:
                lenore.reply_to(message, "I'm sorry Dave, I'm afraid I can't do that.")
    except Exception as e:
        lenore.reply_to(message, e)


@lenore.message_handler(commands=['removewarn'])
def mod_remove_warn(message):
    try:
        cid = message.chat.id
        uid = message.from_user.id  # ид отдающего команду
        if db_func.db_check_chat_table_exists(cid):
            if not db_func.db_check_user_exists(cid, uid):
                db_func.db_add_new_user(cid, uid, info_get_current_username(cid, uid))
            if db_func.db_check_user_have_right(cid, uid, 'warn_func'):
                if message.reply_to_message is None:
                    lenore.delete_message(cid, message.message_id)
                else:
                    ruid = message.reply_to_message.from_user.id  # получаем цель, которую обрабатываем

                    if check_user_is_admin(ruid, cid):  # проверяем цель на админа
                        lenore.delete_message(cid, message.message_id)
                    else:  # если не админ
                        unwarned_user_naming = info_get_current_username(cid, ruid)  # получаем видимое имя пользователя

                        if db_func.db_check_chat_table_exists(cid):  # проверяем, есть ли чат в базе данных
                            if db_func.db_check_user_exists(cid, ruid):  # если есть - проверяем наличие юзера
                                if db_func.db_get_current_warn_info(cid, ruid)[0] > 0:
                                    db_func.db_remove_last_warn(cid, ruid)  # стираем варн
                                    current_warn_count = db_func.db_get_current_warn_info(cid, ruid)[0]

                                    # сообщение для случая, если чат в базе
                                    info_message = 'Предупреждение снято для {0}\n' \
                                                   'Текущее количество предупреждений: {1}'.format(unwarned_user_naming,
                                                                                                   current_warn_count)
                                    # если чата нет в списке на форвард, просто отправляем сообщение
                                    if not check_chat_forwarding(cid):
                                        lenore.reply_to(message, info_message)
                                    # если чат есть в списке на форвард - отправляем сообщение с анварном в чат и пересылаем куда надо
                                    else:
                                        if message.chat.username is None:  # получаем ссылку на чат, чтобы использовать дальше
                                            # noinspection PyUnusedLocal
                                            chat_link = '(приватный чат, ссылка недоступна)'
                                        else:
                                            chat_link = 't.me/' + message.chat.username
                                            # сообщение для пересылки, если чат в базе
                                            forward_message_text = '{0} снял одно предупреждение {1} в чате {2} ({3})'.format(
                                                info_get_current_username(cid, message.from_user.id),
                                                unwarned_user_naming,
                                                message.chat.title,
                                                chat_link)
                                        lenore.reply_to(message, info_message)
                                        lenore.forward_message(check_chat_forwarding(cid), cid,
                                                               message.reply_to_message.message_id)
                                        lenore.send_message(check_chat_forwarding(cid), forward_message_text)
                                else:
                                    lenore.reply_to(message,
                                                    'Мне интересно, как ты собираешься снимать варны у {0}, если их вообще-то нет?'.format(
                                                        unwarned_user_naming))
                            else:
                                lenore.reply_to(message,
                                                'Юзер {0} в базе не зарегистрирован!'.format(
                                                    unwarned_user_naming))

                        else:
                            lenore.send_message(cid, 'Поскольку чат не в базе, варны не считаются и снять их невозможно.')
            else:
                lenore.reply_to(message, "I'm sorry Dave, I'm afraid I can't do that.")
    except Exception as e:
        lenore.reply_to(message, e)


@lenore.message_handler(commands=['pin'])
def mod_pin(message):
    try:
        cid = message.chat.id
        uid = message.from_user.id  # ид отдающего команду
        if db_func.db_check_chat_table_exists(cid):
            if not db_func.db_check_user_exists(cid, uid):
                db_func.db_add_new_user(cid, uid, info_get_current_username(cid, uid))
            if db_func.db_check_user_have_right(cid, uid, 'pin_func'):
                if message.reply_to_message is None:
                    lenore.delete_message(cid, message.message_id)
                else:
                    lenore.delete_message(cid, message.message_id)
                    lenore.pin_chat_message(cid, message.reply_to_message.message_id)
            else:
                lenore.reply_to(message, "I'm sorry Dave, I'm afraid I can't do that.")
    except Exception as e:
        lenore.reply_to(message, e)


@lenore.message_handler(commands=['unpin'])
def mod_unpin(message):
    try:
        cid = message.chat.id
        uid = message.from_user.id  # ид отдающего команду
        if db_func.db_check_chat_table_exists(cid):
            if not db_func.db_check_user_exists(cid, uid):
                db_func.db_add_new_user(cid, uid, info_get_current_username(cid, uid))
            if db_func.db_check_user_have_right(cid, uid, 'pin_func'):
                lenore.delete_message(cid, message.message_id)
                lenore.unpin_chat_message(cid)
            else:
                lenore.reply_to(message, "I'm sorry Dave, I'm afraid I can't do that.")
    except Exception as e:
        lenore.reply_to(message, e)


###
### Технические команды
###
###
### Создание БД
###
# @lenore.message_handler(commands=['init_tables'])
# def tech_init_table(message):
#     try:
#         cid = message.chat.id
#         table_names = ['chat_1001032838103', 'chat_1001085305161', 'chat_1001254199480', 'chat_1001444879250',
#                        'chat_1001450465443', 'chat_1001457973105', 'chat_1001394778416']
#         if message.from_user.id == 24978372:
#             for table in table_names:
#                 if not db_func.db_check_chat_table_exists(cid):
#                     db_func.db_create_table_for_chat(table)
#                 db_func.db_create_tech_table()
#                 lenore.send_message(cid, 'База данных успешно создана')
#     except Exception as e:
#         lenore.reply_to(message, e)


###
### UID и CID
###
@lenore.message_handler(commands=['get_tech'])
def tech_get_chat_info(message):
    try:
        cid = message.chat.id
        if message.from_user.id == 24978372:
            if message.reply_to_message is None:
                uid = message.from_user.id
            else:
                uid = message.reply_to_message.from_user.id
            infostring = "UID: {0}\n CID: {1}\n".format(uid, cid)
            lenore.send_message(cid, infostring)
    except Exception as e:
        lenore.reply_to(message, e)


# ###
# ### Одноразовая функция
# ###
# @lenore.message_handler(commands=['update_tables'])
# def tech_update_tables(message):
#     try:
#         if message.from_user.id == 24978372:
#             res = db_func.db_add_columns_for_administration()
#             lenore.reply_to(message, res)
#     except Exception as e:
#         lenore.reply_to(message, e)
#
#
# ###
# ### Полный доступ к функциям бота
# ###
# @lenore.message_handler(commands=['get_powah'])
# def tech_get_powah(message):
#     try:
#         if message.from_user.id == 24978372:
#             res = db_func.db_set_absolute_power_for_nio()
#             lenore.reply_to(message, res)
#     except Exception as e:
#         lenore.reply_to(message, e)


###
### Запуск таблицы для чата
###
@lenore.message_handler(commands=['init'])
def tech_init_table_or_chat_(message):
    try:
        cid = message.chat.id
        uid = message.from_user.id
        spl = str(message.text).split(' ')
        if message.from_user.id == 24978372:
            if len(spl) > 1:
                if spl[0] == '-':
                    table_name = 'chat_' + str(spl[1])[1:]
                else:
                    table_name = 'chat_' + spl[1]
                if not db_func.db_check_chat_table_exists(cid):
                    db_func.db_create_table_for_chat(table_name)
                    lenore.reply_to(message, 'Таблица {0} создана успешно.'.format(table_name))
                    if not db_func.db_check_user_exists(cid, uid):
                        db_func.db_add_new_user(cid, uid, info_get_current_username(cid, uid))
                    current_user_rights = db_func.db_check_all_user_rights(cid, uid)
                    db_func.db_set_user_rights(cid, uid, 1, 1, 1, 1, 1, 1, 1)
                    new_user_rights = db_func.db_check_all_user_rights(cid, uid)
                    lenore.reply_to(message, 'Права успешно изменены для {0}:\n'
                                             'Было:  {1}\n'
                                             'Стало: {2}'.format(info_get_current_username(cid, uid),
                                                                 current_user_rights,
                                                                 new_user_rights))
                else:
                    lenore.reply_to(message, 'Таблица {0} уже существует'.format(table_name))
            else:
                table_name = 'chat_' + str(cid)[1:]
                if not db_func.db_check_chat_table_exists(cid):
                    db_func.db_create_table_for_chat(table_name)
                    lenore.reply_to(message, 'Таблица {0} создана успешно.'.format(table_name))
                    if not db_func.db_check_user_exists(cid, uid):
                        db_func.db_add_new_user(cid, uid, info_get_current_username(cid, uid))
                    current_user_rights = db_func.db_check_all_user_rights(cid, uid)
                    db_func.db_set_user_rights(cid, uid, 1, 1, 1, 1, 1, 1, 1)
                    new_user_rights = db_func.db_check_all_user_rights(cid, uid)
                    lenore.reply_to(message, 'Права успешно изменены для {0}:\n'
                                             'Было:  {1}\n'
                                             'Стало: {2}'.format(info_get_current_username(cid, uid),
                                                                 current_user_rights,
                                                                 new_user_rights))
                else:
                    lenore.reply_to(message, 'Таблица {0} уже существует'.format(table_name))
    except Exception as e:
        lenore.reply_to(message, e)


###
### Обработка войсов
###
@lenore.message_handler(content_types=['voice'])
def processing_detect_voice(message):
    try:
        if not check_bot_can_delete(message):
            pass
        else:
            lenore.delete_message(message.chat.id, message.message_id)
    except Exception as e:
        lenore.reply_to(message, e)


###
### Сбор статистики
###
@lenore.message_handler(content_types=['text', 'sticker', 'document', 'video'])
def processing_add_stat_info_to_db(message):
    try:
        uid = message.from_user.id
        cid = message.chat.id
        username = info_get_current_username(cid, uid)
        if db_func.db_check_chat_table_exists(cid):
            if db_func.db_check_user_exists(cid, uid):
                db_func.db_update_user_message_count(cid, uid)
            else:
                db_func.db_add_new_user(cid, uid, username)
                db_func.db_update_user_message_count(cid, uid)
        else:
            pass
    except Exception as e:
        lenore.reply_to(message, e)


lenore.polling()
