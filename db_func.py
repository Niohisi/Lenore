#!/usr/bin/python
# coding=utf-8

import sqlite3
import os
import time

working_space = os.path.dirname(os.path.realpath(__file__))
database = os.path.join(working_space, "database.db")


def db_create_table_for_chat(chat_id):
    conn = sqlite3.connect(database)
    cursor = conn.cursor()
    table_name = chat_id
    cursor.execute(
        """CREATE TABLE IF NOT EXISTS {0} 
        (user_id integer, 
        username text, 
        date_added integer, 
        message_count integer, 
        warn_count integer, 
        last_warn_message text, 
        kick_date integer, 
        kick_message text,
        all_actions_allowed integer,
        limited_actions_allowed integer,
        warn_func integer,
        mute_func integer,
        ban_func integer,
        pin_func integer,
        can_change_rights integer)""".format(
            table_name))
    conn.commit()
    conn.close()


def db_create_tech_table():
    conn = sqlite3.connect(database)
    cursor = conn.cursor()
    table_name = 'welcome_messages'
    cursor.execute(
        """
        CREATE TABLE IF NOT EXISTS {0} 
        (chat_id integer, last_finished_welcome_message_id integer)
        """.format(table_name))
    conn.commit()
    conn.close()


def db_check_chat_table_exists(cid):
    conn = sqlite3.connect(database)
    cursor = conn.cursor()
    table_for_use = 'chat_' + str(cid)[1:]
    cursor.execute("""SELECT count(*) FROM sqlite_master WHERE type='table' AND name='{0}'""".format(table_for_use))
    if cursor.fetchone()[0] == 1:
        conn.close()
        return True
    else:
        conn.close()
        return False


def db_check_user_exists(cid, uid):
    conn = sqlite3.connect(database)
    cursor = conn.cursor()
    table_for_use = 'chat_' + str(cid)[1:]
    cursor.execute("""SELECT * FROM '{0}' WHERE user_id = '{1}'""".format(table_for_use, uid))
    data = cursor.fetchone()
    conn.close()
    if data is not None:
        return True
    else:
        return False


def db_add_new_user(cid, uid, username):
    conn = sqlite3.connect(database)
    cursor = conn.cursor()
    table_for_use = 'chat_' + str(cid)[1:]
    current_time = int(time.time())
    message_count = 1
    warn_count = 0
    last_warn_message = 'Empty'
    kick_date = 0
    kick_message = 'Empty'
    all_actions_allowed = 1
    limited_actions_allowed = 0
    warn_func = 0
    mute_func = 0
    ban_func = 0
    pin_func = 0
    can_change_rights = 0
    sql = """INSERT INTO {0} VALUES (
    '{1}', 
    '{2}', 
    '{3}', 
    '{4}', 
    '{5}', 
    '{6}', 
    '{7}', 
    '{8}',
    '{9}',
    '{10}',
    '{11}',
    '{12}',
    '{13}',
    '{14}',
    '{15}')""".format(table_for_use, uid, username, current_time, message_count, warn_count, last_warn_message,
                      kick_date, kick_message, all_actions_allowed, limited_actions_allowed, warn_func, mute_func,
                      ban_func, pin_func, can_change_rights)
    cursor.execute(sql)
    conn.commit()
    conn.close()
    return True


def db_check_last_welcome_message_exists(cid):
    conn = sqlite3.connect(database)
    cursor = conn.cursor()
    cursor.execute("""SELECT * FROM 'welcome_messages' WHERE chat_id = '{0}'""".format(cid))
    data = cursor.fetchone()
    if data is not None:
        conn.close()
        return True
    else:
        conn.close()
        return False


def db_set_last_welcome_message_id(cid, message_id):
    conn = sqlite3.connect(database)
    cursor = conn.cursor()
    cursor.execute("""INSERT INTO welcome_messages VALUES ('{0}','{1}')""".format(cid, message_id))
    conn.commit()
    conn.close()


def db_get_last_welcome_message_id(cid):
    conn = sqlite3.connect(database)
    cursor = conn.cursor()
    cursor.execute("""SELECT last_welcome_message_id FROM 'welcome_messages' WHERE chat_id = '{0}'""".format(cid))
    data = cursor.fetchone()
    conn.close()
    return data[0]


def db_update_last_welcome_message_id(cid, message_id):
    conn = sqlite3.connect(database)
    cursor = conn.cursor()
    cursor.execute(
        """UPDATE welcome_messages SET last_welcome_message_id = '{0}' WHERE chat_id = '{1}'""".format(message_id, cid))
    data = cursor.fetchone()
    conn.commit()
    conn.close()


def db_update_user_message_count(cid, uid):
    conn = sqlite3.connect(database)
    cursor = conn.cursor()
    table_for_use = 'chat_' + str(cid)[1:]
    sql = """
    SELECT message_count 
    FROM '{0}' 
    WHERE user_id = '{1}'
    """.format(table_for_use, uid)
    cursor.execute(sql)
    data = cursor.fetchone()
    foo = int(data[0] + 1)
    sql = """
    UPDATE {0}
    SET message_count = '{1}'
    WHERE user_id = '{2}'
    """.format(table_for_use, foo, uid)
    cursor.execute(sql)
    conn.commit()
    conn.close()
    return True


def db_increase_warn_count(cid, uid, warn_message):
    conn = sqlite3.connect(database)
    cursor = conn.cursor()
    table_for_use = 'chat_' + str(cid)[1:]
    sql = """
        SELECT warn_count, last_warn_message
        FROM '{0}' 
        WHERE user_id = '{1}'
        """.format(table_for_use, uid)
    cursor.execute(sql)
    data = cursor.fetchone()
    foo = int(data[0] + 1)
    sql = """
        UPDATE {0}
        SET warn_count = '{1}',
            last_warn_message = '{2}'
        WHERE user_id = '{3}'
        """.format(table_for_use, foo, warn_message, uid)
    cursor.execute(sql)
    conn.commit()
    conn.close()
    return True


def db_nullify_warn_count(cid, uid):
    conn = sqlite3.connect(database)
    cursor = conn.cursor()
    warn_message = 'Empty'
    table_for_use = 'chat_' + str(cid)[1:]
    foo = 0
    sql = """
        UPDATE {0}
        SET warn_count = '{1}',
            last_warn_message = '{2}'
        WHERE user_id = '{3}'
        """.format(table_for_use, foo, warn_message, uid)
    cursor.execute(sql)
    conn.commit()
    conn.close()
    return True


def db_add_ban_info(cid, uid, ban_message):
    conn = sqlite3.connect(database)
    cursor = conn.cursor()
    table_for_use = 'chat_' + str(cid)[1:]
    sql = """
        UPDATE {0}
        SET kick_date = '{1}',
            kick_message = '{2}'
        WHERE user_id = '{3}'
        """.format(table_for_use, int(time.time()), ban_message, uid)
    cursor.execute(sql)
    conn.commit()
    conn.close()
    return True


def db_get_current_warn_info(cid, uid):
    """
    :rtype: tuple(int warn_count, text last_warn_message)
    """
    conn = sqlite3.connect(database)
    cursor = conn.cursor()
    table_for_use = 'chat_' + str(cid)[1:]
    sql = """
            SELECT warn_count, last_warn_message
            FROM '{0}' 
            WHERE user_id = '{1}'
            """.format(table_for_use, uid)
    cursor.execute(sql)
    data = cursor.fetchone()
    conn.close()
    return data


def db_get_top_flooders(cid):
    conn = sqlite3.connect(database)
    cursor = conn.cursor()
    table_for_use = 'chat_' + str(cid)[1:]
    sql = """
            SELECT username, message_count
            FROM '{0}'
            ORDER BY message_count DESC;            
            """.format(table_for_use)
    cursor.execute(sql)
    data = cursor.fetchall()
    conn.close()
    return data


def db_get_message_count(cid, uid):
    """
    :rtype: tuple(int message_count)
    """
    conn = sqlite3.connect(database)
    cursor = conn.cursor()
    table_for_use = 'chat_' + str(cid)[1:]
    sql = """
            SELECT message_count
            FROM '{0}' 
            WHERE user_id = '{1}'
            """.format(table_for_use, uid)
    cursor.execute(sql)
    data = cursor.fetchone()
    conn.close()
    return data


def db_remove_last_warn(cid, uid):
    conn = sqlite3.connect(database)
    cursor = conn.cursor()
    table_for_use = 'chat_' + str(cid)[1:]
    sql = """
            SELECT warn_count, last_warn_message
            FROM '{0}' 
            WHERE user_id = '{1}'
            """.format(table_for_use, uid)
    cursor.execute(sql)
    data = cursor.fetchone()
    foo = int(data[0] - 1)
    sql = """
            UPDATE {0}
            SET warn_count = '{1}',
                last_warn_message = '{2}'
            WHERE user_id = '{3}'
            """.format(table_for_use, foo, 'Empty', uid)
    cursor.execute(sql)
    conn.commit()
    conn.close()
    return True


def db_add_columns_for_administration():
    conn = sqlite3.connect(database)
    cursor = conn.cursor()
    table_list = ['chat_1001032838103', 'chat_1001085305161', 'chat_1001175459209', 'chat_1001245949155',
                  'chat_1001254199480', 'chat_1001295499832', 'chat_1001328989582', 'chat_1001444879250',
                  'chat_1001450465443', 'chat_1001457973105']
    columns_list = ['all_actions_allowed', 'limited_actions_allowed', 'warn_func', 'mute_func', 'ban_func', 'pin_func',
                    'can_change_rights']
    processed_chats = ''
    for table in table_list:
        for column in columns_list:
            sql = """ALTER TABLE {0} ADD COLUMN {1} integer;""".format(table, column)
            cursor.execute(sql)
            conn.commit()
        sql = """UPDATE {0} SET all_actions_allowed='1',
                    limited_actions_allowed = '0',
                    warn_func = '0',
                    mute_func = '0',
                    ban_func = '0',
                    pin_func = '0',
                    can_change_rights = '0'""".format(table)
        cursor.execute(sql)
        conn.commit()
        processed_chats += '{0} successfully processed\n'.format(table)
    conn.close()
    return processed_chats


def db_set_absolute_power_for_nio(all_actions_allowed=1, limited_actions_allowed=1, warn_func=1, mute_func=1,
                                  ban_func=1, pin_func=1, can_change_rights=1):
    conn = sqlite3.connect(database)
    cursor = conn.cursor()
    table_list = ['chat_1001032838103', 'chat_1001085305161', 'chat_1001175459209', 'chat_1001245949155',
                  'chat_1001254199480', 'chat_1001295499832', 'chat_1001328989582', 'chat_1001444879250',
                  'chat_1001450465443', 'chat_1001457973105']
    processed_chats = ''
    for table in table_list:
        sql = """
                UPDATE {0}
                SET all_actions_allowed = '{1}',
                    limited_actions_allowed = '{2}',
                    warn_func = '{3}',
                    mute_func = '{4}',
                    ban_func = '{5}',
                    pin_func = '{6}',
                    can_change_rights = '{7}'
                WHERE user_id = '24978372'
                """.format(table, all_actions_allowed, limited_actions_allowed, warn_func, mute_func, ban_func,
                           pin_func, can_change_rights)
        cursor.execute(sql)
        conn.commit()
        processed_chats += '{0} successfully owned\n'.format(table)
    conn.close()
    return processed_chats


def db_get_user_id_by_username(cid, username):
    conn = sqlite3.connect(database)
    cursor = conn.cursor()
    table_for_use = 'chat_' + str(cid)[1:]
    sql = """
        SELECT user_id 
        FROM '{0}' 
        WHERE username = '{1}'
        """.format(table_for_use, username)
    cursor.execute(sql)
    data = cursor.fetchone()
    conn.close()
    if data is not None:
        return data[0]
    else:
        return False


def db_set_user_rights(cid, uid, all_actions_allowed=0, limited_actions_allowed=0, warn_func=0, mute_func=0, ban_func=0,
                       pin_func=0, can_change_rights=0):
    conn = sqlite3.connect(database)
    cursor = conn.cursor()
    table_for_use = 'chat_' + str(cid)[1:]
    sql = """
            UPDATE {0}
            SET all_actions_allowed = '{1}',
                limited_actions_allowed = '{2}',
                warn_func = '{3}',
                mute_func = '{4}',
                ban_func = '{5}',
                pin_func = '{6}',
                can_change_rights = '{7}'
            WHERE user_id = '{8}'
            """.format(table_for_use, all_actions_allowed, limited_actions_allowed, warn_func, mute_func, ban_func,
                       pin_func, can_change_rights, uid)
    cursor.execute(sql)
    conn.commit()
    conn.close()
    return True


def db_check_user_have_right(cid, uid, right):
    # all_actions_allowed, limited_actions_allowed, warn_func, mute_func, ban_func, pin_func, can_change_rights
    conn = sqlite3.connect(database)
    cursor = conn.cursor()
    table_for_use = 'chat_' + str(cid)[1:]
    sql = """
    SELECT {0} 
    FROM '{1}' 
    WHERE user_id = '{2}'
    """.format(right, table_for_use, uid)
    cursor.execute(sql)
    data = cursor.fetchone()
    conn.close()
    if data[0] == 1:
        return True
    else:
        return False


def db_check_all_user_rights(cid, uid):
    # all_actions_allowed, limited_actions_allowed, warn_func, mute_func, ban_func, pin_func
    conn = sqlite3.connect(database)
    cursor = conn.cursor()
    table_for_use = 'chat_' + str(cid)[1:]
    sql = """
    SELECT all_actions_allowed, limited_actions_allowed, warn_func, mute_func, ban_func, pin_func, can_change_rights 
    FROM '{0}' 
    WHERE user_id = '{1}'
    """.format(table_for_use, uid)
    cursor.execute(sql)
    data = cursor.fetchone()
    conn.close()
    return data
